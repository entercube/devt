<%--
프로그램 ID 	: SHR-224-A
화일명		: index.jsp
프로그램기능 	: 메인인덱스 
작성일 		: 2015.12.12  
Version		: 0.9 
작성자		: 홍세준(nightfog777@gmail.com)
 
--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
  
<%@page 
 		import="java.util.Locale, java.util.Vector"
 
%>
 
 

<%
	response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1. 
	response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
	response.setDateHeader("Expires", 0);  

	String prgID = "SHR-224-A:index.jspp";
 
 
	// 1. 변수설정 
	request.setCharacterEncoding("utf-8");	
 
 	 
	long timeVal = System.currentTimeMillis(); 
	
%> 


<!DOCTYPE html>
<html>
<head>
	<title>시작</title> 
	
	<meta charset="utf-8">
	<meta id="viewportID" name="viewport" content="user-scalable=no, width=device-width, initial-scale=0.85" >
   
 
	<style type='text/css'>
 
  
	</style>
		
</head>

<body>
 	<h2>Hello!</h2>
 	<p>
 	시간 : <%=timeVal%>
</body>
</html>